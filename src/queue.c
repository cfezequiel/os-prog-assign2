/** 
 * \file    queue.c 
 * \author  Carlos Ezequiel
 * \date    2012-02-26
 */
#include <stdlib.h>
#include <stdio.h>

#include "queue.h"

pthread_mutex_t queue_lock;

void QUEUE_fifo_init(QUEUE_fifo_t *queue, int n)
{
    queue->n = n + 1;
    queue->data = malloc(sizeof(Node_t) * queue->n);
    queue->front = 0;
    queue->end = 0;
}

int QUEUE_fifo_append(QUEUE_fifo_t *queue, Node_t *item)
{
    if ((queue->end + 1) % queue->n == queue->front)
    {
        return -1;
    }
    queue->data[queue->end] = item;
    queue->end = (queue->end + 1) % queue->n;
    return 0;
}

int QUEUE_fifo_pop(QUEUE_fifo_t *queue, Node_t **item)
{
    if (queue->front == queue->end)
    {
        return -1;
    }
    *item = queue->data[queue->front];
    queue->front = (queue->front + 1) % queue->n;
    return 0;
}

void QUEUE_fifo_destroy(QUEUE_fifo_t *queue)
{
    free(queue->data);
}

inline void QUEUE_sem_wait(QUEUE_fifo_t *queue, Node_t *sem)
{
    pthread_mutex_lock(&queue_lock);
    QUEUE_fifo_append(queue, sem);
    pthread_cond_wait(sem, &queue_lock);
    pthread_mutex_unlock(&queue_lock);
}

inline void QUEUE_sem_signal(QUEUE_fifo_t *queue)
{
    Node_t *sem;
    pthread_mutex_lock(&queue_lock);
    QUEUE_fifo_pop(queue, &sem);
    pthread_cond_signal(sem);
    pthread_mutex_unlock(&queue_lock);
}



