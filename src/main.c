/** 
 * \file    main.c 
 * \author  Carlos Ezequiel
 * \date    2012-02-25
 */

#include <stdlib.h>
#include <stdio.h>

#include "defines.h"
#include "input.h"
#include "baboon.h"


int main(int argc, char *argv[])
{
    int n;
    int time;
    int result;
    int *sequence;

    /* Check input parameters */
    if (argc < 3)
    {
        printf("Usage: %s <input file> <time (sec)>\n", argv[0]);
        return EXIT_FAILURE;
    }

    /* Get time parameter */
    time = atoi(argv[2]);

    /* Read input sequence from file */
    n = IN_readFromFile(argv[1], &sequence);
    if (n <= 0)
    {
        printf("ERROR: unable to read from file.\n");
    }

    /* Run the baboon crossing algorithm */
    result = BAB_runBaboons(sequence, n, time);

    return result;
}


