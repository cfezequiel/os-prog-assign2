/** 
 * \file    queue.h 
 * \author  Carlos Ezequiel
 * \date    2012-02-26
 */
#include <pthread.h>

#define Node_t pthread_cond_t

typedef struct 
{
    int n;
    int front;
    int end;
    Node_t **data;

} QUEUE_fifo_t;

void QUEUE_fifo_init(QUEUE_fifo_t *queue, int n);
void QUEUE_fifo_destroy(QUEUE_fifo_t *queue);
void QUEUE_sem_wait(QUEUE_fifo_t *queue, Node_t *sem);
void QUEUE_sem_signal(QUEUE_fifo_t *queue);


