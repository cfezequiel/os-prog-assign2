/** 
 * \file    baboon.h
 * \author  Carlos Ezequiel
 * \date    2012-02-26
 */
#include "defines.h"
#include <pthread.h>

#define BAB_ROPE_CAPACITY   3

#define BAB_STATUS_REQUEST  1
#define BAB_STATUS_GRANTED  2
#define BAB_STATUS_WAIT     3
#define BAB_STATUS_EXIT     4

typedef struct
{
    int direction;
    int index;
    int time;
    int status;
    pthread_cond_t cond;
} BAB_Baboon_t;

typedef struct
{
    int direction;
    int *sequence;
    int index;
    int occupants;
    BAB_Baboon_t *queue[BAB_ROPE_CAPACITY];
    int qEnd;
    int qBeg;
    int qNext;
    pthread_mutex_t lock;
} rope_t;


int BAB_runBaboons(int *sequence, int n, int time);


