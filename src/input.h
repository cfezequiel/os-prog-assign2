/** 
 * \file    input.h
 * \author  Carlos Ezequiel
 * \date    2012-02-25
 */

int IN_readFromFile(const char *filename, int **sequence);
