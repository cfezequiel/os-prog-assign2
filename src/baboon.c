/** 
 * \file    baboon.c 
 * \author  Carlos Ezequiel
 * \date    2012-02-26
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "baboon.h"
#include "queue.h"

char const *strDirections[] = {"left to right", "right to left"};
rope_t rope;
BAB_Baboon_t *baboons;
int nBaboons;
QUEUE_fifo_t waitQueue;
pthread_mutex_t createThread;

void BAB_setStatus(BAB_Baboon_t *baboon, int status)
{
    baboon->status = status;
    switch (baboon->status)
    {
    case BAB_STATUS_REQUEST:
        printf("Baboon %d: Request to cross rope (%s)\n",
                baboon->index + 1, strDirections[baboon->direction]);
        break;

    case BAB_STATUS_GRANTED:
        printf("Baboon %d: Cross rope request granted ", baboon->index + 1);
        printf("(Current crossing: %s, ", strDirections[baboon->direction]);
        printf("Number of baboons on rope: %d)\n", rope.occupants);
        break;

    case BAB_STATUS_WAIT:
        printf("Baboon %d: Waiting\n", baboon->index + 1);
        break;

    case BAB_STATUS_EXIT:
        printf("Baboon %d: Exit rope ", baboon->index + 1);
        printf("(Current crossing: %s, ", strDirections[baboon->direction]);
        printf("Number of baboons on rope: %d)\n", rope.occupants);
        break;

    default:
        printf("ERROR: Unknown status [i] %d", baboon->status);
    }
}

BAB_Baboon_t * BAB_nextBaboon(const BAB_Baboon_t *baboon)
{
    int i = baboon->index;
    return i < nBaboons - 1 ? &baboons[i + 1] : NULL;
}

BAB_Baboon_t * BAB_prevBaboon(const BAB_Baboon_t *baboon)
{
    int i = baboon->index;
    return i > 0 ? &baboons[i - 1] : NULL;
}

void BAB_getOnRope(BAB_Baboon_t *baboon)
{
    BAB_setStatus(baboon, BAB_STATUS_REQUEST);
    while (1)
    {
        pthread_mutex_lock(&rope.lock);
        if (rope.sequence[rope.index] == baboon->direction && 
            rope.direction == baboon->direction)
        {
            if (rope.occupants < BAB_ROPE_CAPACITY)
            {
                rope.occupants++;
                rope.index++;
                rope.queue[rope.qEnd] = baboon;
                rope.qEnd = (rope.qEnd + 1) % BAB_ROPE_CAPACITY;
                BAB_setStatus(baboon, BAB_STATUS_GRANTED);
                pthread_mutex_unlock(&createThread);
                pthread_mutex_unlock(&rope.lock);
                return;
            }
        }

        BAB_setStatus(baboon, BAB_STATUS_WAIT);
        pthread_mutex_unlock(&rope.lock);
        QUEUE_sem_wait(&waitQueue, &baboon->cond);
    }
}

void BAB_getOffRope(BAB_Baboon_t *baboon)
{
    pthread_mutex_lock(&rope.lock);
    rope.occupants--;
    BAB_setStatus(baboon, BAB_STATUS_EXIT);
    if (rope.index == nBaboons)
    {
        return;
    }
    rope.direction = rope.sequence[rope.index];
    if (baboon->index == rope.queue[rope.qBeg]->index)
    {
        rope.qBeg = (rope.qBeg + 1) % BAB_ROPE_CAPACITY;
        pthread_cond_signal(&baboons[rope.qNext].cond);
    }
    else
    {
        rope.qNext = baboon->index;
        pthread_cond_wait(&baboon->cond, &rope.lock);
    }
    if (rope.occupants == 0)
    {
        QUEUE_sem_signal(&waitQueue);
    }
    pthread_mutex_unlock(&rope.lock);
}

void BAB_globalInit(int *sequence, int n)
{
    rope.sequence = sequence;
    rope.direction = sequence[0];
    rope.index = 0;
    rope.direction = sequence[0];
    rope.occupants = 0;
    rope.qEnd = 0;
    rope.qBeg = 0;
    nBaboons = n;
    baboons = malloc(sizeof(BAB_Baboon_t) * n);
    QUEUE_fifo_init(&waitQueue, n);
}

void * BAB_runBaboon(void *attr)
{
    BAB_Baboon_t *baboon = (BAB_Baboon_t *) attr;

    BAB_getOnRope(baboon);
    sleep(baboon->time);
    BAB_getOffRope(baboon);

    return NULL;
}

int BAB_runBaboons(int *sequence, int n, int time)
{
    int i;
    int rc;
    pthread_t *threads;

    /* Initial globals */
    BAB_globalInit(sequence, n);

    /* Create threads */
    threads = malloc(sizeof(pthread_t) * n);
    for (i = 0; i < n; i++)
    {
        baboons[i].direction = sequence[i];
        baboons[i].index = i;
        baboons[i].time = time;
        rc = pthread_create(
                    &threads[i], 
                    NULL, 
                    BAB_runBaboon,
                    (void *) &baboons[i]
                );
        if (rc)
        {
            /* Error */
            return rc;
        }
        pthread_mutex_lock(&createThread);
    }

    /* Wait for all threads to complete */
    for (i = 0; i < n; i++)
    {
        pthread_join(threads[i], NULL);
    }

    /* Release allocated memories */
    free(threads);
    return 0;
}


