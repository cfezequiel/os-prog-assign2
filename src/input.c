/** 
 * \file    input.c 
 * \author  Carlos Ezequiel
 * \date    2012-02-25
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "defines.h"

int IN_readFromFile(const char *filename, int **sequence)
{
    struct stat st;
    char c;
    int i = 0;
    FILE *fp;

    /* Open the file, check for errors */
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        return 0;
    }

    /* Get file size */
    stat(filename, &st);
    *sequence = malloc(sizeof(int) * (st.st_size + 1) / 2);

    /* Read from file */
    while (fscanf(fp, "%c, ", &c) > 0)
    {
        switch (c)
        {
        case 'L':
            (*sequence)[i++] = DIR_LEFT;
            break;

        case 'R':
            (*sequence)[i++] = DIR_RIGHT;
            break;

        case '\n': /* After last valid character reached */
            break;

        default: /* Error, invalid character */
            return 0;
        }
    }

    /* Close file */
    fclose(fp);

    return i;
}


