#! /usr/bin/make

SDIR=src
IDIR=src
BDIR=bin
UDIR=build
VPATH=$(SDIR)
EXECNAME=baboon

# Adjust compiler option based on OS
OS=$(shell uname)
ifeq ($(OS), Linux)
	THREAD_ARG=-pthread
else ifeq ($(OS), SunOS)
	THREAD_ARG=-pthreads
endif

ARGS=-I$(IDIR) -pedantic -Wall $(THREAD_ARG) -g
    
all: $(BDIR)/$(EXECNAME)

$(BDIR)/$(EXECNAME): main.c input.c baboon.c queue.c
	gcc $^ -o $@ $(ARGS) $(OPT_ARGS)

clean:
	rm -rf $(BDIR)/$(EXECNAME)

.PHONY: clean
